# another irc client library
yeah i know


## why "client"?
This is not an end-user client like irssi or hexchat. It's a library that talks to a server, hence "client".


## Quick start
The following code creates an IRC bot which answers to "hello" in `#SpamChannel`.
```javascript
var tls = require('tls'),
    irc = require('./src/client');

var socket = tls.connect(6697, 'irc.rizon.net');
var bot = new irc(socket);

bot.nick('SomeBot');
bot.user('SomeBot', 'another irc bot');
bot.join('#SpamChannel');

bot.on('message', event => {
    // say "hello" and the bot will respond "hello!"
    if (event.message === 'hello')
        bot.send(event.target, 'hello!');
});
```

Mostly follows [RFC 1459](https://tools.ietf.org/html/rfc1459). Check out the [Modern IRC Client Protocol Specification](https://modern.ircdocs.horse/) for additional tips. Does not parse IRCv3 message tags.

## API
See [api.md](api.md) for documentation.
- Class: irc
  - Methods:
    - write
    - pass
    - nick
    - user
    - oper
    - quit
    - join
    - part
    - mode
    - topic
    - names
    - list
    - invite
    - kick
    - send
    - notice
    - who
    - whois
    - whowas
  - Events:
    - `message` on PRIVMSG
    - `notice` on NOTICE
    - `join` on JOIN
    - `part` on PART
    - `quit` on QUIT
    - `welcome` on 001 (RPL_WELCOME)
    - `nick collision` on 433 (ERR_NICKNAMEINUSE) or 436 (ERR_NICKCOLLISION)

- Class: Parser
  - Methods:
    - `new Parser(stream)`
  - Events:
    - `line` on each line of data received from the server
    - `data` when `line` is parsed.
