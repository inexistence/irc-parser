# API
## Class: Client
The `Client` class provides an irc client. It's responsible for abstracting away IRC commands and parsing the incoming network stream into events which can be listened to.

## new Client(stream)
- `stream` [&lt;Stream&gt;](https://nodejs.org/docs/latest/api/stream.html#stream_stream) A stream from [`net`](https://nodejs.org/docs/latest/api/net.html) or [`tls`](https://nodejs.org/docs/latest/api/tls.html).

Example:
```js
var stream = tls.connect(6697, 'irc.freenode.net'),
    client = new Client(stream)
```

This library does not handle socket error events.

## client.write(data)
- `data` &lt;String&gt; A line to be written into the stream.

This allows writing lines directly to the stream.

Example:
```js
client.write('OPER operNick operPassword');
client.write('REHASH');
```

## client.pass(password)
- `password` &lt;String&gt; A password for authentication with the irc network.

Warning: this is the irc `PASS` command, different from services like NickServ or Q.


## client.nick(nickname)
- `nickname` &lt;String&gt; A nickname to be used on the irc network.

Warning: the irc network is free to reject your chosen nick for any reason (against policies, already used, etc). You must listen and handle `nick collision` events.


## client.user(username, realname)
- `username` &lt;String&gt; A username to be used on the irc network.
- `realname` &lt;String&gt; The real name of the user behind the client, for use on the irc network. Most clients do "realname".


## client.oper(nick, pass)
- `nick` &lt;String&gt; Your network operator username.
- `pass` &lt;String&gt; Your network operator password.


## client.quit([reason])
- `reason` &lt;String&gt; *Optional*. Reason for quitting the network.

Warning: some networks disallow `QUIT` reasons if you have been connected for less than a few hours.


## client.join(chans[, keys])
- `chans` &lt;String&gt; Channel *or* comma-separated channels to join.
- `keys` &lt;String&gt; Key or comma-separated keys for the respective channels.

Consult [RFC 1459 § 4.2.1](https://tools.ietf.org/html/rfc1459#section-4.2.1) or the [Modern IRC Client Protocol Specification](https://modern.ircdocs.horse/#join-message) for additional information or examples on comma-separated lists of channels.


## client.part(chans[, reason])
- `chans` &lt;String&gt; Channel *or* comma-separated list of channels to part.
- `reason` &lt;String&gt; Reason for leaving the channel(s).

Consult [RFC 1459 § 4.2.2](https://tools.ietf.org/html/rfc1459#section-4.2.2) or the [modern IRC Client Protocol Specification](https://modern.ircdocs.horse/#part-message) for additional information or examples on comma-separated lists of channels.


## client.mode(target[, modes])
- `target` &lt;String&gt; Target of the command.
- `modes` &lt;String&gt; *Optional*. Modes to be set on `target`.

The `MODE` irc command is used to query, set or remove options from a given target. Consult the (Modern IRC Client Protocol Specification)[https://modern.ircdocs.horse/#mode-message] for details. Keep in mind that the many irc daemons support different modes.


## client.topic(chan[, topic])
- `chan` &lt;String&gt; Channel whose topic is being queried, deleted or set.
- `topic` &lt;String&gt; *Optional*. Topic to be set. If undefined, requests the current topic. If `null`, removes the topic.

Example:
```js
// requests #chan's topic
client.topic('#chan')

// sets #chan's topic to "egg"
client.topic('#chan', 'egg')

// delete #chan's topic
client.topic('#chan', null)
```


## client.names(chans)
- `chans` &lt;String&gt; Channel or comma-separated channels whose names are being requested.


## client.list([chans[, server]])
- `chans` &lt;String&gt; *Optional*. Channel whose status is being queried. Can be comma-separated list.
- `server` &lt;String&gt; *Optional*. The target server.

You must listen to `RPL_LISTSTART`, `RPL_LIST` and `RPL_LISTEND`.


## client.invite(nicks, chan)
- `nicks` &lt;String&gt; Nick or comma-separated nicks who will be invited to `chan`.
- `chan` &lt;String&gt; Channel where `nicks` are being invited.

Warning: some networks don't allow you to mass-invite (so about one invite every few mins).

## client.kick(chan, nick[, reason])
- `chan` &lt;String&gt; The channel where the kick is happening.
- `nick` &lt;String&gt; The nick that is being kicked.
- `reason` &lt;String&gt; *Optional*. Why the nick is being kicked.

You obviously must have op to kick.


## client.send(targets, msg)
- `targets` &lt;String&gt; Channel or nick where the message is to be delivered. Can be a comma-separated list.
- `msg` &lt;String&gt; The content of the message.

While comma-separated lists are supported as per the irc protocol, I'm sure some snowflake network disallows it because of spam.

Example:
```js
// say hi to some user called "wolo"
client.send('wolo', 'hello world')

// say hi to a channel
client.send('#spam', 'hello world')

// send the same message to four nicks at once
client.send('wolo,mango,pong,taiga', 'hello world')
```


## client.notice(targets, msg)
- `targets` &lt;String&gt; Channel or nick where the message is to be delivered. Can be a comma-separated list.
- `msg` &lt;String&gt; The content of the message.


## client.who([mask[, opersonly]])
- `mask` &lt;String&gt; *Optional*. Nick mask, a filter of sorts.
- `opersonly` &lt;Boolean&gt; *Optional*. If `WHO` should only return network operators.


## client.whois(nick)
- `nick` &lt;String&gt; The nick whose information is being requested.


## client.whowas(nick)
- `nick` &lt;String&gt; The nick whose information is being requested.

## Event: `line`
- `line` &lt;String&gt; The irc line. Raw, unparsed, straight from the socket.

Emitted every time the server sends a line. Useful for debugging, but nothing should listen to this event.

## Event: `data`
- `raw` &lt;String&gt; The raw string that was parsed from the incoming data.
- `prefix` &lt;String&gt; The origin of the message, as per rfc1459§2.3.
- `command` &lt;String&gt; The IRC command being issued. ALWAYS present.
- `params` &lt;String&gt; Parameters to the command. Not all commands have arguments.
- `trailing` &lt;String&gt; If valid, the last parameter to `command` that might contain spaces. It's the "user content" on IRC commands like PRIVMSG or NOTICE.

Emitted by `Parser` every time a line is parsed.

## Event: `message`
- `prefix` &lt;String&gt; The origin of the message. Generally, `nick!user@host`.
- `target` &lt;String&gt; The target of the message.
- `msg` &lt;String&gt; Content.

Emitted every time a `PRIVMSG` is received.

## Event: `notice`
- `prefix` &lt;String&gt; The origin of the notice. Generally, `nick!user@host`.
- `target` &lt;String&gt; The target of the notice.
- `msg` &lt;String&gt; Content.

Emitted every time a `NOTICE` is received.

## Event: `join`
- `user` &lt;String&gt; Who joined.
- `chan` &lt;String&gt; What channel they joined.

Emitted every time a `JOIN` is received.

## Event: `part`
- `user` &lt;String&gt; User who parted.
- `chan` &lt;String&gt; What channel they left.
- `reason` &lt;String&gt; Reason for leaving a channel. Not many users write one, although some clients default to things like "Leaving".

Emitted every time a `PART` is received.

## Event: `quit`
- `user` &lt;String&gt; User who quit.
- `reason` &lt;String&gt; User's reason for leaving the network. Some servers set this when the client errors out (ping timeout, remote host closed connection, etc).

Emitted every time a `QUIT` is received.

## Event: `welcome`
Emitted every time a `001` (RPL_WELCOME) is received. This means the server accepted the connection and we're ready to interact with it.

## Event: `nick collision`
Emitted when the server sends `ERR_NICKNAMEINUSE` or `ERR_NICKCOLLISION`. Your client should attempt to `NICK` again, with a different nickname.
