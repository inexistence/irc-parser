const Parser = require('./parser');

class Client extends Parser {
    constructor(stream) {
        super(stream);
        this.stream = stream;
        this.on('data', this._onParserData);
    }

    write(data) {
        if (data.includes('\0') || data.includes('\r') || data.includes('\n'))
            return;

        this.stream.write(data + '\r\n');
    }

    // runs on every line from the socket
    _onParserData(event) {
        let prefix = event.prefix,
            cmd = event.command,
            params = event.params,
            trailing = event.trailing;

        if (cmd === 'PING') {
            this.write('PONG :' + trailing);
            return;
        }

        else if (cmd === 'PRIVMSG') {
            let target = params,
                msg = trailing;

            this.emit('message', prefix, target, msg);
            return;
        }

        else if (cmd === 'NOTICE') {
            let target = params,
                msg = trailing;

            this.emit('notice', prefix, target, msg);
            return;
        }

        else if (cmd === 'JOIN') {
            let user = event.prefix,
                chan = event.params;

            this.emit('join', user, chan);
            return;
        }

        else if (cmd === 'PART') {
            let user = event.prefix,
                chan = event.params,
                reason = event.trailing;

            this.emit('part', user, chan, reason);
            return;
        }

        else if (cmd === 'QUIT') {
            let user = event.prefix,
                reason = event.trailing;

            this.emit('quit', user, reason);
            return;
        }

        else if (cmd === '001') {
            this.emit('welcome');
            return;
        }

        else if (cmd === '433' || cmd === '436') {
            this.emit('nick collision');
            return;
        }
    }

    // RFC 1459 § 4.1.1
    pass(password) {
        this.write('PASS ' + password);
    }

    // RFC 1459 § 4.1.2
    nick(nickname) {
        this.write('NICK ' + nickname);
    }

    // RFC 1459 § 4.1.3
    user(username, realname) {
        this.write('USER ' + username + ' 0 * :' + realname);
    }

    // RFC 1459 § 4.1.5
    oper(name, pass) {
        this.write('OPER ' + name + ' ' + pass);
    }

    // RFC 1459 § 4.1.6
    quit(reason) {
        if (!reason)
            this.write('QUIT');
        else
            this.write('QUIT :' + reason);
    }

    // RFC 1459 § 4.2.1
    join(chans, keys) {
        if (!keys)
            this.write('JOIN ' + chans);
        else
            this.write('JOIN ' + chans + ' ' + keys);
    }

    // RFC 1459 § 4.2.2
    part(chans, reason) {
        if (!reason)
            this.write('PART ' + chans);
        else
            this.write('PART ' + chans + ' :' + reason);
    }

    // RFC 1459 § 4.2.3
    mode(target, modes) {
        if (!modes)
            this.write('MODE ' + target);
        else
            this.write('MODE ' + target + ' ' + modes);
    }

    // RFC 1459 § 4.2.4
    topic(chan, topic) {
        if (typeof topic === 'undefined')
            this.write('TOPIC ' + chan); // query
        else if (topic === null)
            this.write('TOPIC ' + chan + ' :'); // unset
        else
            this.write('TOPIC ' + chan + ' :' + topic); // set
    }

    // RFC 1459 § 4.2.5
    names(chans) {
        this.write('NAMES ' + chans);
    }

    // RFC 1459 § 4.2.6
    list(chans, server) {
        if (chans) {
            if (server)
                this.write('LIST ' + chans + ' ' + server);
            else
                this.write('LIST ' + chans);
        } else {
            this.write('LIST');
        }
    }

    // RFC 1459 § 4.2.7
    invite(nicks, chan) {
        this.write('INVITE ' + nicks + ' ' + chan);
    }

    // RFC 1459 § 4.2.8
    kick(chan, nick, reason) {
        if (!reason)
            this.write('KICK ' + chan + ' ' + nick);
        else
            this.write('KICK ' + chan + ' ' + nick + ' :' + reason);
    }

    // RFC 1459 § 4.4.1
    send(targets, msg) {
        this.write('PRIVMSG ' + targets + ' :' + msg);
    }

    // RFC 1459 § 4.4.2
    notice(targets, msg) {
        this.write('NOTICE ' + targets + ' :' + msg);
    }

    // RFC 1459 § 4.5.1
    who(mask, opers) {
        if (!mask) {
            this.write('WHO');
        } else {
            if (opers)
                this.write('WHO ' + mask + ' o');
            else
                this.write('WHO ' + mask);
        }
    }

    // RFC 1459 § 4.5.2
    whois(nick) {
        this.write('WHOIS ' + nick);
    }

    // RFC 1459 § 4.5.3
    whowas(nick) {
        this.write('WHOWAS ' + nick);
    }
}

module.exports = Client;
