"use strict";

const readline = require('readline');
const EventEmitter = require('events');

/*
 * Wire format in ABNF, from https://modern.ircdocs.horse/
 *   message     =  [ "@" tags SPACE ] [ ":" prefix SPACE ] command
 *                  [ params ] crlf
 *
 *   tags        =  tag *[ ";" tag ]
 *   tag         =  key [ "=" value ]
 *   key         =  [ vendor "/" ] 1*( ALPHA / DIGIT / "-" )
 *   value       =  *valuechar
 *   valuechar   =  <any octet except NUL, BELL, CR, LF, semicolon (`;`) and SPACE>
 *   vendor      =  hostname
 *
 *   prefix      =  servername / ( nickname [ [ "!" user ] "@" host ] )
 *
 *   command     =  1*letter / 3digit
 *
 *   params      =  *( SPACE middle ) [ SPACE ":" trailing ]
 *   nospcrlfcl  =  <any octet except NUL, CR, LF, colon (`:`) and SPACE>
 *   middle      =  nospcrlfcl *( ":" / nospcrlfcl )
 *   trailing    =  *( ":" / " " / nospcrlfcl )
 *
 *
 *   SPACE       =  %x20 *( %x20 )   ; space character(s)
 *   crlf        =  %x0D %x0A        ; "carriage return" "linefeed"
 *
 *
 *
 * Message format in 'pseudo' BNF https://tools.ietf.org/html/rfc1459#section-2.3.1
 *   <message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
 *   <prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
 *   <command>  ::= <letter> { <letter> } | <number> <number> <number>
 *   <SPACE>    ::= ' ' { ' ' }
 *   <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]
 *
 *   <middle>   ::= <Any *non-empty* sequence of octets not including SPACE
 *                  or NUL or CR or LF, the first of which may not be ':'>
 *   <trailing> ::= <Any, possibly *empty*, sequence of octets not including
 *                    NUL or CR or LF>
 *   <crlf>     ::= CR LF
 */

class Parser extends EventEmitter {
	constructor(stream) {
		super();

		let rl = readline.createInterface({ input: stream });
		rl.on('line', line => this.handleLine(line));

		rl.on('close', _ => this.emit('close'));
		rl.on('pause', _ => this.emit('pause'));
		rl.on('resume', _ => this.emit('resume'));

		this.rl = rl;
	}

	handleLine(line) {
		let raw = line,
			prefix,
			command,
			params,
			trailing;

		this.emit('line', line);

		/* incomplete tags support
		servers must retain backwards compatibility, or they would break
		every non-ircv3 client, so not supporting this is not an issue

		// [ "@" tags SPACE ]
		// tags        =  tag *[ ";" tag ]
		// tag         =  key [ "=" value ]
		if (line[0] === '@') {
			// TODO CHANGE .substr TO .substring
			let index = line.indexOf(' '),
				content = line.substr(1, index - 1); // `1` removes "@"

			tags = content.split(';');
			line = line.substr(index + 1); // `1` excludes separating space
		}
		*/

		// [':' <prefix> <SPACE> ]
		if (line[0] === ':') {
			// <SPACE> after <prefix>
			let index = line.indexOf(' ');

			prefix = line.substring(1, index); // 1 removes ":"
			line = line.substring(index + 1); // everything after <SPACE>
		}

		// check if `params` exists by looking for a space after `command`
		// <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]
		let paramsIndex = line.indexOf(' ');

		if (paramsIndex !== -1) {
			command = line.substring(0, paramsIndex);
			params = line.substring(paramsIndex + 1); // `1` excludes space
			line = '';

			// remove <trailing> from <params>
			let trailingIndex = params.indexOf(' :');

			// this is for when <trailing> is somewhere in <params>
			// from rfc1459:
			//   <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]
			//
			//   <middle>   ::= <Any *non-empty* sequence of octets not including SPACE
			//                  or NUL or CR or LF, the first of which may not be ':'>
			if (trailingIndex !== -1) {
				trailing = params.substring(trailingIndex + 2);
				params = params.substring(0, trailingIndex);
			}

			// and this when <trailing> is <params>
			else if (params[0] === ':') {
				trailing = params.substring(1);
				params = '';
			}
		}
		// otherwise `command` is followed by `crlf`:
		//     ... command crlf
		else {
			command = line;
			line = '';
		}

		let message = { raw, prefix, command, params, trailing };
		Object.freeze(message);
		this.emit('data', message);
	}
}

module.exports = Parser;
